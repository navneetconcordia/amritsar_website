import { Layout } from "antd";
import React from "react";
import PageHead from "../Header/PageHead";
import PageSider from "../Sider/PageSider";
import PageContent from "../Content/PageContent";
import PageFooter from "../Footer/PageFooter";

const { Header, Sider, Footer, Content } = Layout;

class PageLayout extends React.Component {
  render() {
    return (
      <div>
        <Layout>
          <PageHead />
          <PageSider history={this.props.history} />
        </Layout>
      </div>
    );
  }
}

export default PageLayout;
