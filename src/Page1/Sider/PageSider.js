import React from 'react';
import './sider.css'
import 'antd/dist/antd.css';
import data from './content.json'
import * as photos from './Photos_and_Content'

import { Layout, Menu } from 'antd';
import PageFooter from '../Footer/PageFooter';
import PageContent from '../Content/PageContent';


const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;


class PageSider extends React.Component {
  constructor() {
    super();
    this.state= {
      collapsed: false,
      text: data.about_amritsar,
      picture: photos.amritsar,
      title: 'About Amritsar'
      // dummyData: ''
    }
    }

    // componentDidMount(){
    //   this.getDataFromBackend();
    // }

    // getDataFromBackend = () => {
    //   axios.get("/demo")
    //   .then(res=> {
    //     if(res.status === 200){
    //       this.setState({
    //         dummyData: res.data
    //       })
    //     }
    //   });      
    // };

  onCollapse = collapsed => {
    this.setState({ collapsed });
  };

  handleMenu = (e) => {
    if(e.key === '3'){
      this.props.history.push('/temple');
    } else if(e.key === '6'){
      this.props.history.push('/border');
    }
  }

  handleButton = (e) => {
    if(e.key === 'history') {
      this.setState({
        text: data.about_amritsar,
        picture: photos.amritsar,
        title: 'About Amritsar'
      })
    }
    else if(e.key === 'language') {
      this.setState({
        text: data.about_language,
        picture: photos.punjabi,
        title: 'Language'
      })
    }
    else if(e.key === 'golden_temple') {
      this.setState({
        text: data.about_golden_temple,
        picture: photos.gt,
        title: 'Golden Temple'
      })
    }
    else if(e.key === 'shaheedan') {
      this.setState({
        text: data.about_shaheedan,
        picture: photos.sh,
        title: 'Shaheedan Sahib'
      })
    }
    else if(e.key === 'jallianwalaBagh') {
      this.setState({
        text: data.about_bagh,
        picture: photos.jallianwala,
        title: 'Jallianwala Bagh'
      })
    }
    else if(e.key === 'border') {
      this.setState({
        text: data.about_border,
        picture: photos.border,
        title: 'Atari Border'
      })
    }
    else if(e.key === 'gndu') {
      this.setState({
        text: data.about_gndu,
        picture: photos.gndu,
        title: 'Guru Nanak Dev University'
      })
    }
    else if(e.key === 'iim') {
      this.setState({
        text: data.about_iim,
        picture: photos.iim,
        title: 'Indian Institute of Management'
      })
    }
    else if(e.key === 'khalsa') {
      this.setState({
        text: data.about_khalsa_college,
        picture: photos.khalsa_college,
        title: 'Khalsa College'
      })
    }
    else if(e.key === 'roadways') {
      this.setState({
        text: data.about_road,
        picture: photos.road,
        title: 'Roadways'
      })
    }
    else if(e.key === 'railways') {
      this.setState({
        text: data.about_rail,
        picture: photos.rail,
        title: 'Amritsar Railway Station'
      })
    }
    else if(e.key === 'airways') {
      this.setState({
        text: data.about_air,
        picture: photos.air,
        title: 'Sri Guru Ram Dass Jee International Airport'
      })
    }
    else if(e.key === 'police') {
      this.setState({
        text: data.about_police,
        picture: photos.police,
        title: 'Police'
      })
    }
    else if(e.key === 'ambulance') {
      this.setState({
        text: data.about_ambulance,
        picture: photos.ambulance,
        title: 'Ambulance'
      })
    }
    else if(e.key === 'fire') {
      this.setState({
        text: data.about_fire,
        picture: photos.fire,
        title: 'Fire'
      })
    }
  }

  render() {
    // if(!this.state.dummyData){
    //   return <Spin size="large" />;
    // }
    return (
      <Layout style={{ minHeight: '100vh' }}>
        <Sider collapsible collapsed={this.state.collapsed} onCollapse={this.onCollapse}>
          <div className="logo" />
          <Menu onClick={this.handleButton} theme="dark"  defaultSelectedKeys={['history']} mode="inline" >
            <Menu.Item key="history">
              <span i class="fa fa-history" aria-hidden="true">  History</span>
            </Menu.Item>
            <Menu.Item key="language">
              <span i class="fa fa-language" aria-hidden="true">  Language</span>
            </Menu.Item>
            <SubMenu
              key="sub1"
              title={

                  <span i class="fas fa-map-marker-alt">  Places to Visit</span>

              }
            >
              <Menu.Item key="golden_temple">Golden Temple</Menu.Item>
              <Menu.Item key="shaheedan">Shaheedan Sahib</Menu.Item>
              <Menu.Item key="jallianwalaBagh">Jallianwala Bagh</Menu.Item>
              <Menu.Item key="border">India-Pakistan Border</Menu.Item>
            </SubMenu>

            <SubMenu
              key="sub3"
              title={
                <span i class="fa fa-university" aria-hidden="true"> Education</span>
              }
            >
              <Menu.Item key="gndu">Guru Nanak Dev University</Menu.Item>
              <Menu.Item key="iim">Indian Institute of Management</Menu.Item>
              <Menu.Item key="khalsa">Khalsa College</Menu.Item>
            </SubMenu>

            <SubMenu
              key="sub2"
              title={
                <span i class="fas fa-plane" aria-hidden="true">  How to reach</span>
              }
            >
              <Menu.Item key="roadways">Roadways</Menu.Item>
              <Menu.Item key="railways">Railways</Menu.Item>
              <Menu.Item key="airways">Airways</Menu.Item>
            </SubMenu>

            <SubMenu
              key="sub3"
              title={
                  <span i class="fas fa-phone-alt" aria-hidden="true">  Helpline</span>
              }
            >
              <Menu.Item key="police">Police</Menu.Item>
              <Menu.Item key="ambulance">Ambulance</Menu.Item>
              <Menu.Item key="fire">Fire</Menu.Item>
            </SubMenu>

          </Menu>
        </Sider>
        
        <Layout className="site-layout">
          <Header className="site-layout-background" style={{ padding: 0 }} >
            <marquee behavior="scroll" direction="left">
              <span className="punjabi-text">
                <h1>ਅੰਮ੍ਰਿਤਸਰ ਸਿਫਤੀ ਦਾ ਘਰ</h1>
              </span>
        </marquee>
          </Header>
          <Content style={{ margin: '0 16px' }}>
          <PageContent title={this.state.title}/>
            <div className="site-layout-background" style={{ padding: 24, minHeight: 360 }}>
            <img src={this.state.picture}></img>
            {this.state.text}
            </div>
          </Content>
          <Footer style={{ textAlign: 'centre'}}>
            <PageFooter />
          </Footer>
        </Layout> 
      </Layout>
    );
  }
}

export default PageSider;
