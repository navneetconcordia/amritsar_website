import React from 'react'
import './content.css'

function PageContent(props) {
    return ( 
        <div>
            <h2 style={{fontFamily: "Georgia"}}>{props.title}</h2>
        </div>
        );
}

export default PageContent;