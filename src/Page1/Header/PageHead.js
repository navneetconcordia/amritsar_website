import { PageHeader } from "antd";
import { Row, Col } from 'antd';
import React from "react";
import "./header.css";
import * as photos from "../Sider/Photos_and_Content";
import { Slide } from "react-slideshow-image";

function PageHead(props) {
  return (
    <div className="page-header-asr">
      <Row>
        <Col span={4}>
          <h1 id="text">Amritsar</h1>
        </Col>
        <Col span={16}>
          <img id="gurus-photo" src={photos.gurus}/>
        </Col>
        <Col span={4}>
          <img id = "khanda" src = {photos.khanda} />
        </Col>
      </Row>
    </div>


    // <div className="page-header-asr">
    //   <h1>Amritsar</h1>
    //   <img src="https://i.dlpng.com/static/png/5081881-the-ten-gurus-of-sikhism-ritiriwaz-sikh-guru-png-894_354_preview.png"></img>
    // </div>
  );
}

export default PageHead;
