import React from "react";
import './footer.css'
import { Layout } from "antd";
import FooterContent from "./FooterContent";
import FootNote from "./FootNote";

const {Content, Footer} = Layout;

class PageFooter extends React.Component {
  render() {
    return (
    <div>
        <Layout>
          <Content>
            <FooterContent />
          </Content>
          <Footer>
            <FootNote />
          </Footer>
    </Layout>
    </div>
    )
  }
}

export default PageFooter;
