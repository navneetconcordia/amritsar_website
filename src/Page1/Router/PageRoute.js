import React from 'react';


import {
    BrowserRouter as Router,
    Switch,
    Route
  } from "react-router-dom";
import GoldenTemple from '../../Page2/GoldenTemple';
import Border from '../../Page3/Border';
import PageLayout from '../Layout/PageLayout';


class PageRoute extends React.Component {

  render() {
    return (
        <Router>
          <div>
            {/* <nav>
              <ul>
                <li>
                  <Link to="/">Photooo</Link>
                </li>
                <li>
                  <Link to="/points">Points</Link>
                </li>
                <li>
                  <Link to="/age">Age</Link>
                </li>
              </ul>
            </nav> */}
    
            {/* A <Switch> looks through its children <Route>s and
                renders the first one that matches the current URL. */}
            <Switch>
              <Route path="/temple" exact component={GoldenTemple}/>
              <Route path="/border" exact component={Border}/>
              <Route path="/" exact component={PageLayout}/>
            </Switch>
          </div>
        </Router>
      );
  }
}

export default PageRoute;