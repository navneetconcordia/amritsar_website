import React from "react";
import PageLayout from "./Page1/Layout/PageLayout";

class App extends React.Component {
  render() {
    return (
      <div>
        <PageLayout />
      </div>
    );
  }
}

export default App;